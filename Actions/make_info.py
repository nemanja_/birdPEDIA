from bs4 import BeautifulSoup
import wikipedia
import urllib
import json
import time
from PyQt5.QtWidgets import QApplication


def get_soup(url,header):
    return BeautifulSoup(urllib.request.urlopen(urllib.request.Request(url,headers=header)),'html.parser')

def make_info(name):
    start = time.perf_counter()
    url = "https://www.google.com/search?q=" + QApplication.instance().indexer[name].species_sci.replace(" ", "_") + "&source=lnms&tbm=isch"
    header = {
        'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
        }
    soup = get_soup(url, header)

    soup_time = time.perf_counter()
    ActualImages = []  # contains the link for Large original images, type of  image
    for a in soup.find_all("div", {"class": "rg_meta"}, limit=3):
        link = json.loads(a.text)["ou"]
        ActualImages.append(link)

    img_time = time.perf_counter()
    wiki_summary = wikipedia.summary(QApplication.instance().indexer[name].species_sci.replace(" ", "_"))
    wiki_time = time.perf_counter()
    info = "<center>" +\
           '<img style="border:50px" src="' + ActualImages[0] + '" width="300" height="300">' + \
           '<img style="border:50px" src="' + ActualImages[1] + '" width="300" height="300">' + \
           '<img style="border:50px" src="' + ActualImages[2] + '" width="300" height="300">' + '<br>'+\
           str("FAMILY: " + QApplication.instance().indexer[name].family) + "<br>" +\
           str("GENUS: " + QApplication.instance().indexer[name].genus) + "<br>" +\
           str("ORDER: " + QApplication.instance().indexer[name].order) + "<br>" +\
           str("ENGLISH NAME: " + QApplication.instance().indexer[name].species_eng) + "<br>" +\
           str("LATIN NAME: " + QApplication.instance().indexer[name].species_sci) + \
           '<p>' + wiki_summary + "</p> <br>" + \
           "<center>"

    print(start, soup_time, img_time, wiki_time)
    return info