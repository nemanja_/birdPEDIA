from PyQt5.QtWidgets import QApplication
from Actions.make_info import make_info


def display_info():
    QApplication.instance().dataView.display(make_info(QApplication.instance().birdList.currentItem().text()))