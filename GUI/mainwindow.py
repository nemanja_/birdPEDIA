from PyQt5.QtWidgets import QMainWindow, QApplication, QDockWidget, QListWidget, QLineEdit, QListWidgetItem
from PyQt5.QtCore import Qt

from Actions.display_info import display_info
from GUI.dataview import DataView


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()

        # WINDOW RELATED SETTINGS
        self.setWindowTitle("Bird pedia")
        self.setMinimumWidth(800)
        self.setMinimumHeight(600)

        # WINDOW RELATED WIDGETS
        self.init_central()
        self.init_listview()

    def init_central(self):
        QApplication.instance().dataView = DataView()
        self.setCentralWidget(QApplication.instance().dataView)

    def init_listview(self):
        QApplication.instance().birdList = QListWidget()
        for key in QApplication.instance().indexer.keys():
            QApplication.instance().birdList.addItem(QListWidgetItem((key)))
        QApplication.instance().birdList.itemDoubleClicked.connect(display_info)

        self.leftDock = QDockWidget()
        self.leftDock.setTitleBarWidget(QLineEdit("Search.."))
        self.leftDock.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.leftDock.setAllowedAreas(Qt.LeftDockWidgetArea)
        self.leftDock.setWidget(QApplication.instance().birdList)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.leftDock)
