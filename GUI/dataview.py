from PyQt5.Qt import QWebEngineView
from PyQt5.QtCore import Qt, QUrl


class DataView(QWebEngineView):

    def __init__(self, path=""):
        super(DataView, self).__init__()
        self.setContextMenuPolicy(Qt.NoContextMenu)
        self.setWindowTitle(path)
        self.load(QUrl.fromLocalFile(path))

    def display(self, name):
        self.setHtml(name)
