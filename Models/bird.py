class Bird:

    __slots__ = ['order', 'family', 'genus', 'species_sci', 'species_eng']

    def __init__(self, order, family, genus, species_sci, species_eng):
        self.order       = order
        self.family      = family
        self.genus       = genus
        self.species_sci = species_sci
        self.species_eng = species_eng

    def __str__(self):
        return "ORDER : " + self.order + "\n" + \
               "FAMILY : " + self.family  + "\n" + \
               "GENUS : " + self.genus  + "\n" + \
               "SCIENTIFIC NAME : " + self.species_sci  + "\n" + \
               "ENGLISH NAME : " + self.species_eng