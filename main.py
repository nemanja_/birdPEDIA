import sys
from pickle import load
from PyQt5.QtWidgets import QApplication
from GUI.mainwindow import MainWindow

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.indexer = load(open('bird-index.avian', 'rb'))
    app.mainWindow = MainWindow()
    app.mainWindow.show()
    sys.exit(app.exec_())